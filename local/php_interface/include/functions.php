<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * Дампит какой-либо массив
 * @param string $var Входная строка
 * @param boolean $die Завершение выполнения скрипта
 * @param boolean $all Доступ любого юзера
 * @return void
 */
function dump($var, $die = false, $all = false)
{
    global $USER;
    if (($USER->GetID() == 1) || ($all == true))
    {
        ?>
        <font style="text-align: left; font-size: 10px"><pre><?var_dump($var)?></pre></font><br>
        <?
    }
    if ($die)
    {
        die;
    }
}